/**
 * @(#) CerradoAlumnos.java
 */

package es.unican.is2.Pra3;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class CerradoAlumnos extends ControlAccesosState{
	int horaApertura = 8;
	int minutosApertura = 0;
	Calendar cal = Calendar.getInstance(); 
	ControlAccesos contextoActual;
	Timer timer;
	TimerTask task;    
	@Override
	public void entryAction(ControlAccesos contexto){
		timer = new Timer();
		task  = new TimerTask() {
	        @Override
	        public void run()
	        {
	        	exitAction(contextoActual);
	        	Abierto abierto = new Abierto();
	        	contextoActual.setState(abierto);
	        	abierto.entryAction(contextoActual);
	        	abierto.doAction(contextoActual);
	        }
	    };
		contexto.bloqueaPuerta();
		contexto.setMensaje("Cerrado a Alumnos");
	    timer.schedule(task, cuantosMSQuedanPaAbrir(horaApertura,minutosApertura));
	    contextoActual = contexto;
	    System.out.println("Cerrado a Alumnos");
	}
	@Override
	public void exitAction(ControlAccesos contexto){}
	@Override
	public void doAction(ControlAccesos contexto){}	
	@Override
	public void tarjetaDetectada(ControlAccesos contexto){
		this.exitAction(contexto);
		ControlAccesosState estadoDestino;		
		if(contexto.getUsuario() instanceof Profesor){			
			//System.out.println("el usuario es profesor");
			estadoDestino=new EsperandoPin();
			contexto.setState(estadoDestino);
			estadoDestino.entryAction(contexto);
			estadoDestino.doAction(contexto);
		}		
	}	
	@Override
	public void aperturaEmergencia(ControlAccesos contexto){
		Abierto estadoDestino=new Abierto();
		this.exitAction(contexto);
		contexto.setState(estadoDestino);	
		estadoDestino.entryAction(contexto); 
		estadoDestino.doAction(contexto);
	}	
	public int horaActual(){
		return cal.get(Calendar.HOUR_OF_DAY);
	}
	public int minutosActuales(){
		return cal.get(Calendar.MINUTE);
	}
	public int segActuales(){
		return cal.get(Calendar.SECOND);
	}
	public int cuantosMSQuedanPaAbrir(int horaApertura, int minutosApertura){
		int horaActual = horaActual();
		int minutosActuales = minutosActuales();
		int minApertura = horaApertura * 60;
		minApertura += minutosApertura;
		horaActual *= 60;
		minutosActuales += horaActual;
		int dif = minApertura - minutosActuales;
		dif *= 60;
		dif -= segActuales();
		dif*=1000;
		if(dif < 0){
			dif += 86400000; // ms de un dia
		}
		return dif;
	}	
}
/**
 * @(#) UsuariosDAO.java
 */

package es.unican.is2.Pra3;

import java.util.ArrayList;

public class UsuariosDAO
{
	static ArrayList<Usuario> usuarios=new ArrayList<Usuario>();
	

	public Usuario buscaUsuario( String nombreUsuario )
	{

			for(int i=0; i<usuarios.size(); i++){
				if(usuarios.get(i).getNombre().equals(nombreUsuario)){
					return usuarios.get(i);
				}
			}
			
			return null;
	}
	
	public void anadeUsuario(Usuario user){
		usuarios.add(user);
		
	}
	
	
}

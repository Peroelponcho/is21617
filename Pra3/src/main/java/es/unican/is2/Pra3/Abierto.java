/**
 * @(#) Abierto.java
 */

package es.unican.is2.Pra3;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class Abierto extends ControlAccesosState{
	int horaCierre = 20;
	int minutosCierre = 0;
	Calendar cal = Calendar.getInstance(); 
	ControlAccesos contextoActual;
	Timer timer;
	TimerTask task;    
	@Override
	public void entryAction(ControlAccesos contexto){
		timer = new Timer();
		task  = new TimerTask() {
	        @Override
	        public void run()
	        {
	        	exitAction(contextoActual);
	        	CerradoAlumnos cerrado = new CerradoAlumnos();
	        	contextoActual.setState(cerrado);
	        	cerrado.entryAction(contextoActual);
	        	cerrado.doAction(contextoActual);
	        }
	    };
		contexto.desbloqueaPuerta();
		contexto.setMensaje("Abierto");
	    timer.schedule(task, cuantosMSQuedanPaCerrar(horaCierre,minutosCierre));
	    contextoActual = contexto;
	    System.out.println("Abierto");		
	}
	@Override
	public void exitAction(ControlAccesos contexto){}
	@Override
	public void doAction(ControlAccesos contexto){}
	public int horaActual(){
		return cal.get(Calendar.HOUR_OF_DAY);
	}
	public int minutosActuales(){
		return cal.get(Calendar.MINUTE);
	}
	public int segActuales(){
		return cal.get(Calendar.SECOND);
	}
	public int cuantosMSQuedanPaCerrar(int horaCierre, int minutosCierre){
		int horaActual = horaActual();
		int minutosActuales = minutosActuales();
		int minCierre = horaCierre * 60;
		minCierre += minutosCierre;
		horaActual *= 60;
		minutosActuales += horaActual;
		int dif = minCierre - minutosActuales;
		dif *= 60;
		dif -= segActuales();
		dif*=1000;
		if(dif < 0){
			dif += 86400000; // ms de un dia
		}
		return dif;
	}	
}
/**
 * @(#) AperturaTemporal.java
 */

package es.unican.is2.Pra3;

import java.util.Timer;
import java.util.TimerTask;

public class AperturaTemporal extends ControlAccesosState{
	ControlAccesos contextoActual;
	Timer timer;
	TimerTask task;    
	@Override
	public void entryAction(ControlAccesos contexto){
		timer = new Timer();
		task  = new TimerTask() {
	        @Override
	        public void run()
	        {
	        	exitAction(contextoActual);
	        	CerradoAlumnos cerrado = new CerradoAlumnos();
	        	contextoActual.setState(cerrado);
	        	cerrado.entryAction(contextoActual);
	        	cerrado.doAction(contextoActual);
	        }
	    };
		contexto.setMensaje("Apertura Temporal");
	    timer.schedule(task, 120000);
	    contextoActual = contexto;
	    System.out.println("Apertura Temporal");
	}
	@Override
	public void exitAction(ControlAccesos contexto){
		timer.cancel();
	}
	@Override
	public void doAction(ControlAccesos contexto){}	
	@Override
	public void aperturaEmergencia( ControlAccesos contexto )
	{
		Abierto estadoDestino=new Abierto();
		this.exitAction(contexto);
		contexto.setState(estadoDestino);	
		estadoDestino.entryAction(contexto); 
		estadoDestino.doAction(contexto);

	}	
}
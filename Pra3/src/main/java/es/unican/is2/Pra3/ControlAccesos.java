/**
 * @(#) ControlAccesos.java
 */

package es.unican.is2.Pra3;

import java.util.Date;

/**
 * Clase principal del sistema de control de accesos a la facultad.
 */
public class ControlAccesos
{
	/**
	 * Usuario correspondiente a la tarjeta detectada
	 */
	private Usuario usuario;
	
	/**
	 * Hora de apertura de la facultad
	 */
	private Date horaApertura;
	
	/**
	 * Hora de cierra de la facultad
	 */
	private Date horaCierre;
	
	/**
	 * Intervalo de tiempo (en segundos) que permanecen las puertas desbloqueadas para permitir el acceso.
	 */
	private int intervaloApertura;
	
	/**
	 * Intervalo de tiempo (en segundos) durante los que el usuario detectado puede introducir su pin.
	 */
	private int intervaloPin;
	
	/**
	 * Mensaje que se muestra en el panel
	 */
	private String mensaje;
	
	
	private UsuariosDAO usuariosDao;
	
	private ControlAccesosState estado;
	
	/**
	 * Operaci�n que se ejecuta para bloquear las puertas. 
	 */
	public void bloqueaPuerta( )
	{
		
	}
	
	/**
	 * Operaci�n que se ejecuta para desbloquear las puertas.
	 */
	public void desbloqueaPuerta( )
	{
		
	}
	
	public void setState( ControlAccesosState estado )
	{
		this.estado=estado;
		
	}
	
	public void setMensaje(String mensaje){
		this.mensaje=mensaje;
		
	}
	
	public Usuario getUsuario(){
		return usuario;
		
	}
	
	public void setUsuario(Usuario user){
		this.usuario=user;
	}

	public boolean pinCorrecto() {
		Usuario usuarioVerificar=this.usuariosDao.buscaUsuario(this.usuario.getNombre());
		//System.out.println(usuarioVerificar.getNombre()+"  "+ usuarioVerificar.getPin());
		//System.out.println(this.usuario.getNombre()+"  "+ this.usuario.getPin());
		return usuarioVerificar.getPin().equals(this.usuario.getPin());

	}

	public String getMensaje() {
		return mensaje;
	}
	
	public void setUsuariosDao(UsuariosDAO usuarios){
		this.usuariosDao=usuarios;
	}
	
	public ControlAccesosState getEstado(){
		return this.estado;
	}
	

	

	
	
}

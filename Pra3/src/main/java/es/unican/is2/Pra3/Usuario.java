/**
 * @(#) Usuario.java
 */

package es.unican.is2.Pra3;

public abstract class Usuario
{
	private String nombreUsuario;
	
	private String pin;
	
	public Usuario(String nombre, String pin){
		this.nombreUsuario=nombre;
		this.pin=pin;
	}

	public String getNombre() {
		return this.nombreUsuario;
	}
	
	
	public void setPin(String pin){
		this.pin=pin;
	}

	public String getPin() {
		return pin; 
	}
	
}



/**
 * @(#) EsperandoPin.java
 */

package es.unican.is2.Pra3;

public class EsperandoPin extends ControlAccesosState{
	@Override
	public void entryAction(ControlAccesos contexto){
		contexto.setMensaje("Detectada tarjeta de profesor. Introduzca PIN: ");
	    System.out.println("Esperando Pin");
	}
	@Override
	public void exitAction(ControlAccesos contexto){}
	@Override
	public void doAction(ControlAccesos contexto){}	
	@Override
	public void pinIntroducido(ControlAccesos contexto){		
		this.exitAction(contexto);
		AperturaTemporal estadoDestino=new AperturaTemporal();
		if(contexto.pinCorrecto()){
			contexto.setState(estadoDestino);
			estadoDestino.entryAction(contexto); 
			estadoDestino.doAction(contexto);
		}		
	}	
	@Override
	public void aperturaEmergencia(ControlAccesos contexto){
		Abierto estadoDestino=new Abierto();
		this.exitAction(contexto);
		contexto.setState(estadoDestino);	
		estadoDestino.entryAction(contexto); 
		estadoDestino.doAction(contexto);
	}	
}
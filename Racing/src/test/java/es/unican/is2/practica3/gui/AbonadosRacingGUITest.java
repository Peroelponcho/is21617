package es.unican.is2.practica3.gui;

import org.fest.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AbonadosRacingGUITest {

	private FrameFixture demo;

	@Before
	public void setUp() {
		CalculoPrecioAbonoGUI sut = new CalculoPrecioAbonoGUI();
		demo = new FrameFixture(sut);
		sut.setVisible(true);
	}

	@After
	public void tearDown() {
		demo.cleanUp();
	}

	@Test
	public void test() {
		// Comprobamos que la interfaz tiene el aspecto deseado
		demo.button("btnCalcular").requireText("CALCULAR");

		try {
			CalculoPrecioAbonoGUI.main(new String[] {});
			// Caso de prueba correcto
			demo.comboBox("comboZona").selectItem("PREFERENCIA_ESTE_OESTE");
			demo.textBox("txtFechaAltaAbonado").setText("11/07/1977");
			demo.textBox("txtFechaNacimiento").setText("11/07/1957");
			demo.radioButton("rdbtnDesempleado").click();
			// Sleeps para ralentizar la ejecuci�n
			Thread.sleep(2000);
			// Pulsamos el bot�n para calcular
			demo.button("btnCalcular").click();
			// Comprobamos la salida
			demo.textBox("txtPrecioAbono").requireText("127.5");

			Thread.sleep(2000);
			
			// Caso de prueba con fecha incorrecta
			demo.textBox("txtFechaAltaAbonado").setText("2015/02/11");
			demo.textBox("txtFechaNacimiento").setText("2015");
			demo.radioButton("rdbtnDesempleado").click();
			Thread.sleep(2000);
			// Pulsamos el bot�n para calcular
			demo.button("btnCalcular").click();
			// Comprobamos la salida
			demo.textBox("txtPrecioAbono").requireText("Fecha Incorrecta");
			Thread.sleep(2000);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

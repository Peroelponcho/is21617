package es.unican.is2.Racing;

import static org.junit.Assert.*;

import org.junit.Test;

import es.unican.is2.practica3.abonadosRacing.Tarifas_2016_2017;
import es.unican.is2.practica3.abonadosRacing.Zona;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Tarifas_2016_2017Test {
	@Test
	public void testPrecioAbono() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		try {
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_SUR_NORTE,sdf.parse("2006"),sdf.parse("2002"),false)==42.5);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_ESTE_OESTE,sdf.parse("1999"),sdf.parse("1994"),true)==93.925);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.PREFERENCIA_ESTE_OESTE,sdf.parse("1977"),sdf.parse("1957"),true)==127.5);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.PREFERENCIA_SUR_NORTE,sdf.parse("1937"),sdf.parse("1921"),false)==78);
			
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_CENTRAL,sdf.parse("1967"),sdf.parse("1952"),false)==286);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_CENTRAL,sdf.parse("1968"),sdf.parse("1953"),false)==330);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_CENTRAL,sdf.parse("1992"),sdf.parse("1992"),false)==330);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_CENTRAL,sdf.parse("1993"),sdf.parse("1993"),false)==128.35);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_CENTRAL,sdf.parse("2007"),sdf.parse("1999"),false)==128.35);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_CENTRAL,sdf.parse("2015"),sdf.parse("2000"),false)==100);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_CENTRAL,sdf.parse("2016"),sdf.parse("2011"),false)==100);
			
			
			
			
			
			
			
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_ESTE_OESTE,sdf.parse("2016"),sdf.parse("2011"),false)==65);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_ESTE_OESTE,sdf.parse("2016"),sdf.parse("1997"),false)==130);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_ESTE_OESTE,sdf.parse("2016"),sdf.parse("1987"),false)==285);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_ESTE_OESTE,sdf.parse("2016"),sdf.parse("1952"),false)==200);
			
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_SUR_NORTE,sdf.parse("2016"),sdf.parse("1999"),false)==115);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_SUR_NORTE,sdf.parse("2016"),sdf.parse("1992"),false)==235);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.TRIBUNA_SUR_NORTE,sdf.parse("2016"),sdf.parse("1952"),false)==175);
			
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.PREFERENCIA_ESTE_OESTE,sdf.parse("2016"),sdf.parse("2011"),false)==40);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.PREFERENCIA_ESTE_OESTE,sdf.parse("2016"),sdf.parse("1999"),false)==80);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.PREFERENCIA_ESTE_OESTE,sdf.parse("2016"),sdf.parse("1952"),false)==100);

			assertTrue(Tarifas_2016_2017.precioAbono(Zona.PREFERENCIA_SUR_NORTE,sdf.parse("2016"),sdf.parse("2011"),false)==30);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.PREFERENCIA_SUR_NORTE,sdf.parse("2016"),sdf.parse("1999"),false)==92);
			assertTrue(Tarifas_2016_2017.precioAbono(Zona.PREFERENCIA_SUR_NORTE,sdf.parse("2016"),sdf.parse("1992"),false)==145);
		} catch (ParseException e) {}
	}
}

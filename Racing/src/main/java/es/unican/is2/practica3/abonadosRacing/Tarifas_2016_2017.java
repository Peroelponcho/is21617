package es.unican.is2.practica3.abonadosRacing;

import java.time.LocalDate;
import java.util.Date;

public class Tarifas_2016_2017 {
	
	@SuppressWarnings("deprecation")
	public static double precioAbono(Zona zona, Date fechaAlta, Date fechaNacimiento,  boolean desempleado) {
		double precio = -1;
		LocalDate ahora = LocalDate.now();
		int edad = ahora.getYear()-fechaNacimiento.getYear()-1900;
		int antiguedad = ahora.getYear()-fechaAlta.getYear()-1900;
		switch(zona){
		case TRIBUNA_CENTRAL:
			if(6 <= edad && edad <= 17){
				precio = 100;
			} else if(18 <= edad && edad <= 24){
				precio = 151;
			} else if(25 <= edad && edad <= 64){
				precio = 440;
			} else if(edad >= 65){
				precio = 440;
			}
			break;
		case TRIBUNA_ESTE_OESTE:
			if(6 <= edad && edad <= 17){
				precio = 65;
			} else if(18 <= edad && edad <= 24){
				precio = 130;
			} else if(25 <= edad && edad <= 64){
				precio = 285;
			} else if(edad >= 65){
				precio = 200;
			}
			break;
		case TRIBUNA_SUR_NORTE:
			if(6 <= edad && edad <= 17){
				precio = 50;
			} else if(18 <= edad && edad <= 24){
				precio = 115;
			} else if(25 <= edad && edad <= 64){
				precio = 235;
			} else if(edad >= 65){
				precio = 175;
			}
			break;
		case PREFERENCIA_ESTE_OESTE:
			if(6 <= edad && edad <= 17){
				precio = 40;
			} else if(18 <= edad && edad <= 24){
				precio = 80;
			} else if(25 <= edad && edad <= 64){
				precio = 200;
			} else if(edad >= 65){
				precio = 100;
			}
			break;
		case PREFERENCIA_SUR_NORTE:
			if(6 <= edad && edad <= 17){
				precio = 30;
			} else if(18 <= edad && edad <= 24){
				precio = 92;
			} else if(25 <= edad && edad <= 64){
				precio = 145;
			} else if(edad >= 65){
				precio = 120;
			}
			break;
		default:
			break;			
		}
		if(antiguedad >= 50){
			precio = precio * 0.65;
		} else if(25 <= antiguedad && antiguedad <= 49){
			precio = precio * 0.75;
		} else if(10 <= antiguedad && antiguedad <= 24){
			precio = precio * 0.85;
		}
		if(desempleado){
			precio = precio * 0.85;
		}
		return precio;
	}

}

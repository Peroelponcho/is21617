package es.unican.is2.practica3.abonadosRacing;

public enum Zona {
	
	TRIBUNA_CENTRAL, TRIBUNA_SUR_NORTE, TRIBUNA_ESTE_OESTE, PREFERENCIA_SUR_NORTE, PREFERENCIA_ESTE_OESTE

}

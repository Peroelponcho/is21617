package es.unican.is2.practica3.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import es.unican.is2.practica3.abonadosRacing.Tarifas_2016_2017;
import es.unican.is2.practica3.abonadosRacing.Zona;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class CalculoPrecioAbonoGUI extends JFrame {

	private JPanel contentPane;
	private JTextField txtFechaNacimiento;
	private JTextField txtFechaAltaAbonado;
	private JTextField txtPrecioAbono;
	private JRadioButton rdbtnDesempleado;
	private JComboBox comboZona;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CalculoPrecioAbonoGUI frame = new CalculoPrecioAbonoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CalculoPrecioAbonoGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 489, 213);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtFechaNacimiento = new JTextField();
		txtFechaNacimiento.setName("txtFechaNacimiento");
		txtFechaNacimiento.setBounds(124, 8, 86, 20);
		contentPane.add(txtFechaNacimiento);
		txtFechaNacimiento.setColumns(10);
		
		JLabel lblFechaNacimiento = new JLabel("Fecha Nacimiento");
		lblFechaNacimiento.setBounds(10, 11, 114, 14);
		contentPane.add(lblFechaNacimiento);
		
		JLabel lblFechaAltaAbonado = new JLabel("Fecha Alta Abonado");
		lblFechaAltaAbonado.setBounds(10, 42, 114, 14);
		contentPane.add(lblFechaAltaAbonado);
		
		txtFechaAltaAbonado = new JTextField();
		txtFechaAltaAbonado.setName("txtFechaAltaAbonado");
		txtFechaAltaAbonado.setBounds(124, 39, 86, 20);
		contentPane.add(txtFechaAltaAbonado);
		txtFechaAltaAbonado.setColumns(10);
		
		rdbtnDesempleado = new JRadioButton("Desempleado");
		rdbtnDesempleado.setName("rdbtnDesempleado");
		rdbtnDesempleado.setBounds(233, 7, 109, 23);
		contentPane.add(rdbtnDesempleado);
		
		comboZona = new JComboBox();
		comboZona.setName("comboZona");
		comboZona.setModel(new DefaultComboBoxModel(new String[] {"TRIBUNA_CENTRAL", "TRIBUNA_SUR_NORTE", "TRIBUNA_ESTE_OESTE", "PREFERENCIA_SUR_NORTE", "PREFERENCIA_ESTE_OESTE"}));
		comboZona.setBounds(243, 39, 210, 20);
		contentPane.add(comboZona);
		
		JLabel lblPrecioAbono = new JLabel("PRECIO ABONO");
		lblPrecioAbono.setBounds(104, 140, 126, 17);
		contentPane.add(lblPrecioAbono);
		
		txtPrecioAbono = new JTextField();
		txtPrecioAbono.setName("txtPrecioAbono");
		txtPrecioAbono.setBounds(217, 138, 166, 20);
		contentPane.add(txtPrecioAbono);
		txtPrecioAbono.setColumns(10);
		
		JButton btnCalcular = new JButton("CALCULAR");
		btnCalcular.setName("btnCalcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
			    Date fechaAlta = null;
			    Date fechaNacimiento = null;
				try {
					fechaAlta = formatter.parse(txtFechaAltaAbonado.getText());
					fechaNacimiento =  formatter.parse(txtFechaNacimiento.getText());
				
				Zona z = Zona.valueOf(comboZona.getSelectedItem().toString());
				double precio=Tarifas_2016_2017.precioAbono(z, fechaAlta,fechaNacimiento,rdbtnDesempleado.isSelected());
				txtPrecioAbono.setText(Double.toString(precio));
				} catch (ParseException e) {
					txtPrecioAbono.setText("Fecha Incorrecta");
				}
			}
		});
		btnCalcular.setBounds(170, 83, 126, 29);
		contentPane.add(btnCalcular);
	}
}
